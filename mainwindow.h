#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class QImage;
class QPoint;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QImage rgb2gray(QImage);
    QImage diff(QImage, QImage);
    void movement(QImage, QImage);
    QPoint findVector(QImage, QImage);
    int diffSum(QImage, QImage);
    QImage diffPositive(QImage, QImage);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::MainWindow *ui;
    QString file1;
    QString file2;
    QImage im;
    QImage diffIm;
    QImage diffPosiIm;
};

#endif // MAINWINDOW_H
