#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QImage>
#include <QFileDialog>
#include <QPoint>
#include <QtMath>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent):QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    this->file1 = "";
    this->file2 = "";
}

MainWindow::~MainWindow() {
    delete ui;
}

QImage MainWindow::rgb2gray(QImage image) {
    QImage img = image;
    for(int i = 0; i < image.width(); i ++) {
        for(int j = 0; j < image.height(); j ++) {
            img.setPixel(i, j, qRgb(qRed(img.pixel(i, j)), qRed(img.pixel(i, j)), qRed(img.pixel(i, j))));
        }
    }
    return img;
}

QImage MainWindow::diff(QImage image1, QImage image2) {
    QImage diffImg = image1;
    for(int i = 0; i < image1.width(); i ++) {
        for(int j = 0; j < image1.height(); j ++) {
            int grayDiff = abs(qRed(image1.pixel(i, j)) - qRed(image2.pixel(i, j)));
            diffImg.setPixel(i, j, qRgb(grayDiff, grayDiff, grayDiff));
        }
    }
    return diffImg;
}

QImage MainWindow::diffPositive(QImage image1, QImage image2) {
    QImage diffImg = image1;
    for(int i = 0; i < image1.width(); i ++) {
        for(int j = 0; j < image1.height(); j ++) {
            int grayDiff = qRed(image1.pixel(i, j)) - qRed(image2.pixel(i, j));
//            qDebug()<<grayDiff;
            int k = 0;
            if(grayDiff > 0) {
                k = 1;
            }
            diffImg.setPixel(i, j, qRgb(0, 0, k));
        }
    }
    return diffImg;
}

void MainWindow::movement(QImage image1, QImage image2) {
    for(int i = 16; i < image1.width() - 15; i += 16) {
        for(int j = 16; j < image1.height() - 15; j += 16) {
            QImage image0 = QImage(QSize(16, 16), QImage::Format_RGB32);
            QImage imageB = QImage(QSize(32, 32), QImage::Format_RGB32);
            for(int m = 0; m < 16; m ++) {
                for(int n = 0; n < 16; n ++) {
                    image0.setPixel(m, n, image1.pixel(i + m, j + n));
                }
            }
            for(int m = - 16; m < 16; m ++) {
                for(int n = - 16; n < 16; n ++) {
                    imageB.setPixel(16 + m, 16 + n, image2.pixel(i + m, j + n));
                }
            }
            QPoint vec = this->findVector(image0, imageB);
//            this->diff(image0, imageB).pixel()
            QImage imagef = QImage(QSize(16, 16), QImage::Format_RGB32);
            for(int m = 0; m < 16; m ++) {
                for(int n = 0; n < 16; n ++) {
                    imagef.setPixel(m, n, imageB.pixel(vec.x() + m + 16, vec.y() + n + 16));
                }
            }
            for(int m = 0; m < 16; m ++) {
                for(int n = 0; n < 16; n ++) {
                    this->diffIm.setPixel(i + m, j + n, this->diff(image0, imagef).pixel(m, n));
                    this->diffPosiIm.setPixel(i + m, j + n,this->diffPositive(image0, imagef).pixel(m, n));
                }
            }
            for(int m = 0; m < 16; m ++) {
                for(int n = 0; n < 16; n ++) {
                    this->im.setPixel(i + m, j + n, image2.pixel(i + vec.x() + m,  j + vec.y() + n) + this->diffIm.pixel(i + m, j + n) * (qBlue(this->diffPosiIm.pixel(i +m, j + n)) * 2 - 1));
                }
            }
//            qDebug()<<vec.x()<<" "<<vec.y()<<endl;
            this->ui->graphicsView_3->scene()->addLine(i, j, vec.x() + i, vec.y() + j, QPen(Qt::red));
            this->ui->graphicsView_3->scene()->addEllipse(i, j, 2, 2, QPen(Qt::red), QBrush(Qt::red));
        }
    }
    for(int i = 0; i < this->im.width(); i ++) {
        for(int j = this->im.height() - 15; j < this->im.height() - 15; j ++) {
            this->im.setPixel(i, j, image1.pixel(i, j));
        }
    }
    for(int i = 0; i < this->im.width(); i ++) {
        for(int j = 0; j < 16; j ++) {
            this->im.setPixel(i, j, image1.pixel(i, j));
        }
    }
    for(int i = 0; i < 16; i ++) {
        for(int j = 0; j < this->im.height(); j ++) {
            this->im.setPixel(i, j, image1.pixel(i, j));
        }
    }
    for(int i = this->im.width() - 15; i < this->im.width(); i ++) {
        for(int j = 0; j < this->im.height(); j ++) {
            this->im.setPixel(i, j, image1.pixel(i, j));
        }
    }
}

QPoint MainWindow::findVector(QImage image0, QImage imageB) {
    QPoint p = QPoint(0, 0);
    int sum = 9999999;
    for(int i = 0; i < image0.width() + 1; i ++) {
        for(int j = 0; j < image0.height() + 1; j ++) {
            QImage imagef = QImage(QSize(16, 16), QImage::Format_RGB32);
            for(int m = 0; m < 16; m ++) {
                for(int n = 0; n < 16; n ++) {
                    imagef.setPixel(m, n, imageB.pixel(i + m, j + n));
                }
            }
//            qDebug()<<i<<" "<<j<<" "<<this->diffSum(image0, imagef);
            if(this->diffSum(image0, imagef) < sum) {
                p = QPoint(i - 16 , j - 16);
                sum = this->diffSum(image0, imagef);
//                qDebug()<<p;
            }
        }
    }
    return p;
}

int MainWindow::diffSum(QImage image1, QImage image2) {
    QImage imageD = this->diff(image1, image2);
    int sum = 0;
    for(int i = 0; i < imageD.width(); i ++) {
        for(int j = 0; j < imageD.height(); j ++) {
            sum += qRed(imageD.pixel(i, j)) * qRed(imageD.pixel(i, j));
        }
    }
    return sum;
}

void MainWindow::on_pushButton_clicked() {
    this->file1 = QFileDialog::getOpenFileName(NULL,"Open File Dialog", NULL, "JPEG(*.jpg);; PNG(*.png);; BMP(*.bmp);; all (*.*)");
    if(this->file1 != "") {
        QImage image1 = QImage(this->file1);
        this->ui->graphicsView->setScene(new QGraphicsScene());
        this->ui->graphicsView->scene()->addPixmap(QPixmap::fromImage(image1));
    }
}

void MainWindow::on_pushButton_2_clicked() {
    this->file2 = QFileDialog::getOpenFileName(NULL,"Open File Dialog", NULL, "JPEG(*.jpg);; PNG(*.png);; BMP(*.bmp);; all (*.*)");
    if(this->file2 != "") {
        QImage image2 = QImage(this->file2);
        this->ui->graphicsView_2->setScene(new QGraphicsScene());
        this->ui->graphicsView_2->scene()->addPixmap(QPixmap::fromImage(image2));
    }
}

void MainWindow::on_pushButton_3_clicked() {
    QImage image1 = QImage(this->file1);
    QImage image2 = QImage(this->file2);
    this->im = QImage(QSize(image1.width(), image1.height()), QImage::Format_RGB32);
    this->diffIm = QImage(QSize(image1.width(), image1.height()), QImage::Format_RGB32);
    this->diffPosiIm = QImage(QSize(image1.width(), image1.height()), QImage::Format_RGB32);
    image1 = this->rgb2gray(image1);
    image2 = this->rgb2gray(image2);
    this->ui->graphicsView_3->setScene(new QGraphicsScene());
    QGraphicsPixmapItem* imItem = this->ui->graphicsView_3->scene()->addPixmap(QPixmap::fromImage(image1));
    this->ui->graphicsView_3->setRenderHint(QPainter::Antialiasing);
    this->movement(image2, image1);
    this->ui->graphicsView_4->setScene(new QGraphicsScene());
    this->ui->graphicsView_4->scene()->addPixmap(QPixmap::fromImage(this->diffIm));
    imItem->setPixmap(QPixmap::fromImage(this->im));
}
